﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public InstanceController instance;
    public GameManager manager;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            instance.SwitchHands();
        }

        if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            manager.LoadRandomQuestion();
        }
    }
}