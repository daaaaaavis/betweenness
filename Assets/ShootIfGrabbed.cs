﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootIfGrabbed : MonoBehaviour
{
    public OVRGrabbable ovrGrabbable;
    public OVRInput.Button shootingButton;

    // Update is called once per frame
    void Update()
    {
        if (ovrGrabbable.isGrabbed && OVRInput.GetDown( shootingButton))
        {
            Debug.Log("Triggered");
            GetComponent<Animator>().SetTrigger("Fire");
        }
    }
}
