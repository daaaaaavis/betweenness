﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndInputController : MonoBehaviour
{
    public InstanceController instance;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            instance.SwitchHands();
        }
    }
}
