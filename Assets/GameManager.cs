﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.VR;
using System.Linq;
using System.Collections.Generic;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public static System.Random rnd;
    private static int questionCount = 21;
    public string StartOfTest; 

    void Awake()
    {
        if (instance != null && instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        rnd = new System.Random();
        DontDestroyOnLoad(gameObject);
        StartOfTest = DateTime.Now.ToString("h-mm-ss");
        UnityEngine.XR.InputTracking.Recenter();
    }

    
    private List<int> questionsToBeHad = Enumerable.Range(1, questionCount).ToList<int>();
    // private int[] completedQuestions = new int[21];

    public void StartTest()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ResetCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadRandomQuestion()
    {
        if (questionsToBeHad.Count() <= 0)
        {
            this.loadFinalScene();   
        }

        int arrayIndex = rnd.Next(0, questionsToBeHad.Count);
        Debug.Log("current array index :" + arrayIndex);
        loadNextScene(arrayIndex);
        questionsToBeHad.RemoveAt(arrayIndex);
    }

    private void loadNextScene(int arrayIndex)
    {
        SceneManager.LoadScene(questionsToBeHad[arrayIndex]);
    }

    private void loadFinalScene()
    {
        SceneManager.LoadScene(22);
    }
}
