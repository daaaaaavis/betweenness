﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OVR;

public class InstanceController : MonoBehaviour
{
    public GameObject leftController;
    public GameObject rightController;
    public GameObject leftHand;
    public GameObject rightHand;
    private bool isController = true;

    public void SwitchHands()
    {
        if (isController)
        {
            leftHand.SetActive(true);
            rightHand.SetActive(true);
            leftController.SetActive(false);
            rightController.SetActive(false);
            isController = false;
        }
        else
        {
            leftHand.SetActive(false);
            rightHand.SetActive(false);
            leftController.SetActive(true);
            rightController.SetActive(true);
            isController = true;
        }
    }    
}
