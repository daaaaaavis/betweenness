﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStart : MonoBehaviour
{
    public static GameStart instance;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private readonly int questionCount = 21;
    private List<int> questionsToBeHad = Enumerable.Range(1, 21).ToList<int>();

    public void StartTest()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ResetCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void LoadRandomQuestion()
    {
        System.Random rnd = new System.Random();
        int arrayIndex = rnd.Next(0, questionsToBeHad.Count);
        Debug.Log("current array index :" + arrayIndex);
        SceneManager.LoadScene(SceneManager.GetSceneByBuildIndex(questionsToBeHad[arrayIndex]).buildIndex);
        questionsToBeHad.RemoveAt(arrayIndex);
    }
}
