﻿using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class QuestionManager : MonoBehaviour
{
    GameManager manager;
    private GameObject answerPanel;
    private Slider slider;
    private GameObject acceptPanel;
    private Slider acceptSlider;
    private float longPressThreshold = 2;
    private int answerValue = 0;
    private float counter;
    private Text holdToAnswer;

    void Start()
    {
        answerPanel = GameObject.Find("LoadingPanel");
        slider = answerPanel.GetComponentInChildren<Slider>();
        acceptPanel = GameObject.Find("AcceptPanel");
        acceptSlider = acceptPanel.GetComponentInChildren<Slider>();
        holdToAnswer = GameObject.Find("HoldToAnswer").GetComponent<Text>();

        GameObject gameObj = GameObject.Find("GameManager");
        if (gameObj == null)
        {
            manager = new GameManager();
        }
        else
        {
            manager = gameObj.gameObject.GetComponent<GameManager>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            // palielināt
            if (answerValue < 6)
            {
                answerValue++;
            }
        }

        if (OVRInput.GetDown(OVRInput.Button.Three))
        {
            // samazināt
            if (answerValue > 0)
            {
                answerValue--;
            }
        }

        slider.value = answerValue;
        Debug.Log(answerValue);

        bool isLeftIndexPressed = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.Touch) == 1.0f;
        bool isRightIndexPressed = OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger, OVRInput.Controller.Touch) == 1.0f;

        if (isLeftIndexPressed && isRightIndexPressed)
        {
            // loaderis, lai saglabātu atbildi
            acceptPanel.SetActive(true);
            holdToAnswer.enabled = false;
            acceptSlider.value = counter / longPressThreshold;
            counter += Time.deltaTime;
            if (counter >= longPressThreshold)
            {
                writeAnswer(answerValue.ToString());
                manager.LoadRandomQuestion();
            }
        } else
        {
            acceptPanel.SetActive(false);
            holdToAnswer.enabled = true;
            counter = 0;
        }
    }

    private void writeAnswer(string answer)
    {
        string text = buildOutput(answer);
        string path = manager.StartOfTest + ".txt";
        if (!File.Exists(path))
        {
            File.Create(path);
        }
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine(text);
        writer.Close();
    }

    private string buildOutput(string answer)
    {
        string output = SceneManager.GetActiveScene().name + ": " + answer;
        output += System.Environment.NewLine + "Time taken : " + Time.timeSinceLevelLoad + System.Environment.NewLine;
        return output;
    }
}
